public class Array {

        // *** 4. Given an array of 10 integers. To display the sum of all its elements except the first and last. ***

//    public static void main(String[] args) {
//        int []stars = new int[]{1500,5,12,56,44,556,987,45,66,500};
//        int sum = 0;
//        for (int i =1; i<9; i++) {
//            sum = sum + stars[i];
//        }
//        System.out.print(sum);
//
//    }

       // -------------------------------------------------------------------

       // *** 5. Write a function which calculates and returns the minimum of 4 arguments. ***

//    public static void main(String[] args) {
//
//        int totalResult = multABCD(3, 10);
//        System.out.println("\n\nMultiplication result: " + totalResult);
//    }
//
//    public static int multABCD(int c, int d) {
//        int resultCD = c * d;
//        return resultCD * multAB(5, 6);
//    }
//
//    public static int multAB(int a, int b) {
//        int resultAB = a * b;
//        return resultAB;
//    }

     // -------------------------------------------------------------------

     // *** 6. Write a method named getEvenDigitSum with one parameter of the type "int". ***
     //        The method should return the sum of even digits within the number.
     //        If the number is negative, method should return -1;

//    public static void main(String[] args) {
//        System.out.println(getEvenDigitSum(1865232));}
//
//    public static int getEvenDigitSum ( int digit){
//        System.out.println("\nDigit for sum even numbers is " + digit);
//        int sumOfEven = 0;
//        char[] numbers = String.valueOf(digit).toCharArray();
//        if (digit >= 0) {
//            for (char element : numbers) {
//                if (element % 2 == 0) {
//                    sumOfEven += Character.getNumericValue(element);
//                }
//            }
//            return sumOfEven;
//        }
//        return -1;
//    }

    // -------------------------------------------------------------------

    // *** 7. Write a method named getGreatestCommonDivisor with two parameters of type int ***
    //        named first and second.
    //        If one of the parameters is < 10, method should return -1 to indicate an invalid value.
    //        The method should return the greatest common divisor of two numbers.

//    public static void main(String[] args)
//    {
//        int first = 50, second = 100;
//        System.out.println(getGreatestCommonDivisor(first, second));
//    }
//
//    public static int getGreatestCommonDivisor (int first, int second)
//    {
//        if (first<10 || second<10) {
//            return -1;
//        }
//        else {
//
//            if (first == second)
//                return first;
//
//            if (first > second)
//                return getGreatestCommonDivisor(first - second, second);
//            return getGreatestCommonDivisor(first, second - first);
//        }
//    }


    // -------------------------------------------------------------------

    // *** 8. Write a program which prints the next figure to the console: ***
    //            **********
    //            **********
    //            **********
    //            **********

//    public static void main(String[] args)
//    {
//        int rows = 4;
//        int stars = 10;
//        for(int i=0;i <rows;i++)
//        {
//            for(int j=0;j<stars;j++)
//            {
//                System.out.print("* ");
//            }
//            System.out.print("\n");
//        }
//    }

    // -------------------------------------------------------------------

    // *** 9. Write a program which prints the next figure to the console: ***
    //            *
    //            **
    //            ***
    //            ****
    //            *****
    //            ******
    //            *******

//    public static void main(String[] args) {
//        int n = 7;
//        printStars(n);
//    }
//    public static void printStars(int n)
//    {
//        int i, j;
//        for(i=0; i<n; i++)
//        {
//            for(j=0; j<=i; j++)
//            {
//                System.out.print("*");
//            }
//            System.out.println();
//        }
//    }

    // -------------------------------------------------------------------

    // *** 10*. Write a program which prints the next figure to the console: ***
    //            **********
    //            *      *
    //            *      *
    //            *      *
    //            **********

//    public static void main(String[] args) {
//
//        for (int i = 1; i<=5; i++) {
//            for (int j = 1; j <=10; j++) {
//                if (i == 1 || i == 5)
//                    System.out.print("*");
//                else if (j == 1 || j == 5)
//                    System.out.print("*");
//                else
//                    System.out.print(" ");
//            }
//            System.out.println();
//        }
//    }

    // -------------------------------------------------------------------

    // *** 11*. Write a program which prints the next figure to the console: ***
//              *
//              ***
//             *****
//             *******
//            *********
//            ***********
//
//    public static void main(String[] args) {
//        int h=6;  // строки
//        int w=11; // ряды
//
//        for (int i = 1; i<=h; i++) {
//            for (int j = 1; j <=w; j++) {
//                  //  System.out.print(".");
//                if (i == 6 && j <=11)
//                    System.out.print("*");
//                if (i == 5 && j <=9)
//                    System.out.print("*");
//                if (i == 4 && j == 2)
//                    System.out.print("*");
//                if (i == 4 && j == 3)
//                    System.out.print("*");
//                if (i == 4 && j == 4)
//                    System.out.print("*");
//                if (i == 4 && j == 5)
//                    System.out.print("*");
//                if (i == 4 && j == 6)
//                    System.out.print("*");
//                if (i == 4 && j == 7)
//                    System.out.print("*");
//                if (i == 4 && j == 8)
//                    System.out.print("*");
//                if (i == 3 && j == 2)
//                    System.out.print("*");
//                if (i == 3 && j == 3)
//                    System.out.print("*");
//                if (i == 3 && j == 4)
//                    System.out.print("*");
//                if (i == 3 && j == 5)
//                    System.out.print("*");
//                if (i == 3 && j == 6)
//                    System.out.print("*");
//                if (i == 2 && j == 3)
//                    System.out.print("*");
//                if (i == 2 && j == 4)
//                    System.out.print("*");
//                if (i == 2 && j == 5)
//                    System.out.print("*");
//                if (i == 1 && j == 3)
//                    System.out.print("*");
//                if (i <=4 && j == 1)
//                    System.out.print(" ");
//                if (i <=2 && j == 2)
//                    System.out.print(" ");
//            }
//            System.out.println(" ");
//        }
//    }
    public static void main(String[] args) {
        //getEvenDigitSum(1321321321);
        System.out.println(getEvenDigitSum(68686));}

       public static int getEvenDigitSum ( int digit){
            System.out.println("\nDigit for sum even numbers is " + digit);
            int sumOfEven = 0;
            char[] numbers = String.valueOf(digit).toCharArray();
            if (digit >= 0) {
                for (char element : numbers) {
                    if (element % 2 == 0) {
                        sumOfEven += Character.getNumericValue(element);
                    }
                }
                return sumOfEven;
            }
            return -1;
        }



}







